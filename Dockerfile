FROM maven as dependencies
WORKDIR /code
ADD pom.xml /code/pom.xml
RUN mvn dependency:go-offline

FROM dependencies as LINTER
WORKDIR /code
ADD pom.xml /code/pom.xml
ADD src /code/src
RUN mvn pmd:check

FROM LINTER as BUILDER
WORKDIR /code
ADD pom.xml /code/pom.xml
ADD src /code/src
RUN mvn install

FROM openjdk:14-jdk-alpine
ARG JAR_FILE=target/*.jar
COPY --from=BUILDER /code/target/game.jar game.jar
COPY --from=BUILDER /code/target/site site
COPY --from=BUILDER /code/target/pit-reports pit-reports
ENTRYPOINT ["java","-jar","/game.jar"]