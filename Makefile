DOCKER_IMAGE=$(shell echo $${DOCKER_IMAGE:-ciklum/game_challenge})
NUM_EXECUTIONS=$(shell echo $${NUM_EXECUTIONS:-100})
FIXED_VALUE=$(shell echo $${NUM_EXECUTIONS:-ROCK})
GAME_MODE=$(shell echo $${GAME_MODE:-RSP})

lint:
	docker build --target LINTER -t $(DOCKER_IMAGE) .

build:
	docker build -t $(DOCKER_IMAGE) .

run:
	docker run $(DOCKER_IMAGE) --numExecutions=${NUM_EXECUTIONS} --fixedValue=${FIXED_VALUE} --gameMode=${GAME_MODE}

