package com.ciklum.challenge.game.utils;

import org.junit.jupiter.api.Test;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThan;

public class TestIntegerRandomGenerator {

    private IntegerRandomGenerator underTest = new IntegerRandomGenerator();

    @Test
    public void testShouldReturnBoundedRandomValue() {
        assertThat(underTest.get(10), lessThan(10));
    }

    @Test
    public void testShouldReturnRandomValues() {
        Set<Integer> result = IntStream.range(0,10).mapToObj(i -> underTest.get(10)).collect(Collectors.toSet());
        assertThat(result.size(), greaterThan(1));
    }
}
