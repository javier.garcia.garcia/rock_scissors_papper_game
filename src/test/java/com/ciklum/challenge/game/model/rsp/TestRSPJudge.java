package com.ciklum.challenge.game.model.rsp;

import com.ciklum.challenge.game.model.base.Hand;
import com.ciklum.challenge.game.model.exceptions.GameException;
import org.junit.jupiter.api.Test;

import static com.ciklum.challenge.game.model.output.Result.*;
import static com.ciklum.challenge.game.model.rsp.RSPHand.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestRSPJudge {

    private RSPJudge underTest = new RSPJudge();

    @Test
    public void testShouldRockLossAgainstPapper() {
        assertThat(underTest.decide(ROCK, PAPPER), equalTo(PLAYER_1_LOSS));
    }

    @Test
    public void testShouldRockWinAgainstScissors() {
        assertThat(underTest.decide(ROCK, SCISSORS), equalTo(PLAYER_1_WINS));
    }

    @Test
    public void testShouldRockDrawAgainstRockr() {
        assertThat(underTest.decide(ROCK, ROCK), equalTo(DRAW));
    }

    @Test
    public void testShouldPapperLossAgainstScissors() {
        assertThat(underTest.decide(PAPPER, SCISSORS), equalTo(PLAYER_1_LOSS));
    }

    @Test
    public void testShouldPapperWinAgainstRock() {
        assertThat(underTest.decide(PAPPER, ROCK), equalTo(PLAYER_1_WINS));
    }

    @Test
    public void testShouldPapperDrawAgainstPapper() {
        assertThat(underTest.decide(PAPPER, PAPPER), equalTo(DRAW));
    }

    @Test
    public void testShouldScissorsLossAgainstRock() {
        assertThat(underTest.decide(SCISSORS, ROCK), equalTo(PLAYER_1_LOSS));
    }

    @Test
    public void testShouldScissorsWinAgainstPapper() {
        assertThat(underTest.decide(SCISSORS, PAPPER), equalTo(PLAYER_1_WINS));
    }

    @Test
    public void testShouldScissorsDrawAgainstScissors() {
        assertThat(underTest.decide(SCISSORS, SCISSORS), equalTo(DRAW));
    }

    @Test
    public void testShouldThrowExceptionWhenP1HandIsWrong() {

        Exception exception = assertThrows(GameException.class, () -> underTest.decide(new OtherHand(), ROCK));

        String expectedMessage = "Invalid hand";
        String actualMessage = exception.getMessage();

        assertThat(actualMessage, equalTo(expectedMessage));
    }

    @Test
    public void testShouldThrowExceptionWhenP2HandIsWrong() {

        Exception exception = assertThrows(GameException.class, () -> underTest.decide(ROCK, new OtherHand()));

        String expectedMessage = "Invalid hand";
        String actualMessage = exception.getMessage();

        assertThat(actualMessage, equalTo(expectedMessage));
    }

    @Test
    public void testShouldThrowExceptionWhenBothHandsAreWrong() {

        Exception exception = assertThrows(GameException.class, () -> underTest.decide(new OtherHand(), new OtherHand()));

        String expectedMessage = "Invalid hand";
        String actualMessage = exception.getMessage();

        assertThat(actualMessage, equalTo(expectedMessage));
    }


    private class OtherHand implements Hand {

    }
}
