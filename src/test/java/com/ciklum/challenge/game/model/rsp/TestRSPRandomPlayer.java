package com.ciklum.challenge.game.model.rsp;

import com.ciklum.challenge.game.utils.IntegerRandomGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.ciklum.challenge.game.model.rsp.RSPHand.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@ExtendWith(MockitoExtension.class)
public class TestRSPRandomPlayer {
    @Mock
    private IntegerRandomGenerator randomGeneratorMock;

    @InjectMocks
    private RSPRandomPlayer underTest;

    @Test
    public void testShouldPlayerReturnRock() {
        Mockito.when(randomGeneratorMock.get(3)).thenReturn(0);
        assertThat(underTest.play(), equalTo(ROCK));
    }

    @Test
    public void testShouldPlayerReturnScissors() {
        Mockito.when(randomGeneratorMock.get(3)).thenReturn(1);
        assertThat(underTest.play(), equalTo(SCISSORS));
    }

    @Test
    public void testShouldPlayerReturnPapper() {
        Mockito.when(randomGeneratorMock.get(3)).thenReturn(2);
        assertThat(underTest.play(), equalTo(PAPPER));
    }
}
