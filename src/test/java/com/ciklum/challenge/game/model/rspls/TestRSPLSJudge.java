package com.ciklum.challenge.game.model.rspls;

import com.ciklum.challenge.game.model.base.Hand;
import com.ciklum.challenge.game.model.exceptions.GameException;
import org.junit.jupiter.api.Test;

import static com.ciklum.challenge.game.model.output.Result.*;
import static com.ciklum.challenge.game.model.rspls.RSPSLHand.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestRSPLSJudge {

    private RSPLSJudge underTest = new RSPLSJudge();

    @Test
    public void testShouldRockLossAgainstPapper() {
        assertThat(underTest.decide(ROCK, PAPPER), equalTo(PLAYER_1_LOSS));
    }

    @Test
    public void testShouldRockLossAgainstSpock() {
        assertThat(underTest.decide(ROCK, SPOCK), equalTo(PLAYER_1_LOSS));
    }

    @Test
    public void testShouldRockWinAgainstScissors() {
        assertThat(underTest.decide(ROCK, SCISSORS), equalTo(PLAYER_1_WINS));
    }

    @Test
    public void testShouldRockWinAgainstLizzard() {
        assertThat(underTest.decide(ROCK, LIZZARD), equalTo(PLAYER_1_WINS));
    }

    @Test
    public void testShouldRockDramAgainstRock() {
        assertThat(underTest.decide(ROCK, ROCK), equalTo(DRAW));
    }

    @Test
    public void testShouldPapperDrawAgainstPapper() {
        assertThat(underTest.decide(PAPPER, PAPPER), equalTo(DRAW));
    }

    @Test
    public void testShouldPapperLossAgainstScissors() {
        assertThat(underTest.decide(PAPPER, SCISSORS), equalTo(PLAYER_1_LOSS));
    }

    @Test
    public void testShouldPapperLossAgainstLizzard() {
        assertThat(underTest.decide(PAPPER, LIZZARD), equalTo(PLAYER_1_LOSS));
    }

    @Test
    public void testShouldPapperWinAgainstRock() {
        assertThat(underTest.decide(PAPPER, ROCK), equalTo(PLAYER_1_WINS));
    }

    @Test
    public void testShouldPapperWinsAgainstSpock() {
        assertThat(underTest.decide(PAPPER, SPOCK), equalTo(PLAYER_1_WINS));
    }

    @Test
    public void testShouldScissorsDrawAgainstScissors() {
        assertThat(underTest.decide(SCISSORS, SCISSORS), equalTo(DRAW));
    }

    @Test
    public void testShouldScissorsLossAgainstRock() {
        assertThat(underTest.decide(SCISSORS, ROCK), equalTo(PLAYER_1_LOSS));
    }

    @Test
    public void testShouldScissorsLossAgainstSpock() {
        assertThat(underTest.decide(SCISSORS, SPOCK), equalTo(PLAYER_1_LOSS));
    }

    @Test
    public void testShouldScissorsWinAgainstPapper() {
        assertThat(underTest.decide(SCISSORS, PAPPER), equalTo(PLAYER_1_WINS));
    }

    @Test
    public void testShouldScissorsWinsAgainstLizzard() {
        assertThat(underTest.decide(SCISSORS, LIZZARD), equalTo(PLAYER_1_WINS));
    }

    @Test
    public void testShouldSpockDrawAgainstSpock() {
        assertThat(underTest.decide(SPOCK, SPOCK), equalTo(DRAW));
    }

    @Test
    public void testShouldSpocksLossAgainstPapper() {
        assertThat(underTest.decide(SPOCK, PAPPER), equalTo(PLAYER_1_LOSS));
    }

    @Test
    public void testShouldSpockLossAgainstLizzard() {
        assertThat(underTest.decide(SPOCK, LIZZARD), equalTo(PLAYER_1_LOSS));
    }

    @Test
    public void testShouldSpockWinAgainstRock() {
        assertThat(underTest.decide(SPOCK, ROCK), equalTo(PLAYER_1_WINS));
    }

    @Test
    public void testShouldSpockWinsAgainstScissors() {
        assertThat(underTest.decide(SPOCK, SCISSORS), equalTo(PLAYER_1_WINS));
    }

    @Test
    public void testShouldLizzardDrawAgainstLizzard() {
        assertThat(underTest.decide(LIZZARD, LIZZARD), equalTo(DRAW));
    }

    @Test
    public void testShouldLizzardsLossAgainstScissors() {
        assertThat(underTest.decide(LIZZARD, SCISSORS), equalTo(PLAYER_1_LOSS));
    }

    @Test
    public void testShouldLizzardLossAgainstRock() {
        assertThat(underTest.decide(LIZZARD, ROCK), equalTo(PLAYER_1_LOSS));
    }

    @Test
    public void testShouldLizzardWinAgainstSpock() {
        assertThat(underTest.decide(LIZZARD, SPOCK), equalTo(PLAYER_1_WINS));
    }

    @Test
    public void testShouldLizzardWinsAgainstPapper() {
        assertThat(underTest.decide(LIZZARD, PAPPER), equalTo(PLAYER_1_WINS));
    }

    @Test
    public void testShouldThrowExceptionWhenP1HandIsWrong() {

        Exception exception = assertThrows(GameException.class, () -> underTest.decide(new OtherHand(), ROCK));

        String expectedMessage = "Invalid hand";
        String actualMessage = exception.getMessage();

        assertThat(actualMessage, equalTo(expectedMessage));
    }

    @Test
    public void testShouldThrowExceptionWhenP2HandIsWrong() {

        Exception exception = assertThrows(GameException.class, () -> underTest.decide(ROCK, new OtherHand()));

        String expectedMessage = "Invalid hand";
        String actualMessage = exception.getMessage();

        assertThat(actualMessage, equalTo(expectedMessage));
    }

    @Test
    public void testShouldThrowExceptionWhenBothHandsAreWrong() {

        Exception exception = assertThrows(GameException.class, () -> underTest.decide(new OtherHand(), new OtherHand()));

        String expectedMessage = "Invalid hand";
        String actualMessage = exception.getMessage();

        assertThat(actualMessage, equalTo(expectedMessage));
    }


    private class OtherHand implements Hand {

    }
}
