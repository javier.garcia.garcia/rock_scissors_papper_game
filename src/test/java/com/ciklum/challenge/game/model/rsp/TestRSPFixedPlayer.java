package com.ciklum.challenge.game.model.rsp;

import com.ciklum.challenge.game.model.exceptions.GameException;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static com.ciklum.challenge.game.model.rsp.RSPHand.SCISSORS;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestRSPFixedPlayer {

    @Test
    public void testShouldFixedPlayerReturnScissors() {
        RSPFixedPlayer underTest = new RSPFixedPlayer("SCISSORS", null);
        assertThat(underTest.play(), equalTo(SCISSORS));
    }

    @Test
    public void testShouldFixedPlayerReturnRandom() {
        RSPRandomPlayer mock = Mockito.mock(RSPRandomPlayer.class);
        Mockito.when(mock.play()).thenReturn(SCISSORS);
        RSPFixedPlayer underTest = new RSPFixedPlayer("RANDOM", mock);
        assertThat(underTest.play(), equalTo(SCISSORS));
        Mockito.verify(mock).play();
    }

    @Test
    public void testShouldFailWhenInvalidFixedValue() {
        Exception exception = assertThrows(GameException.class, () -> new RSPFixedPlayer("A", null));
        String expectedMessage = "Fixed value must be ROCK, SCISSORS, PAPPER or RANDOM";
        String actualMessage = exception.getMessage();

        assertThat(actualMessage, equalTo(expectedMessage));
    }
}
