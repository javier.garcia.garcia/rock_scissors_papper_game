package com.ciklum.challenge.game.model.rsp;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;

public class TestRSPHand {

    @Test
    public void testShouldReturRock() {
        assertThat(RSPHand.from(0), equalTo(RSPHand.ROCK));
    }

    @Test
    public void testShouldReturScissors() {
        assertThat(RSPHand.from(1), equalTo(RSPHand.SCISSORS));
    }

    @Test
    public void testShouldReturPapper() {
        assertThat(RSPHand.from(2), equalTo(RSPHand.PAPPER));
    }

    @Test
    public void testShouldReturNull() {
        assertThat(RSPHand.from(3), nullValue());
    }
}
