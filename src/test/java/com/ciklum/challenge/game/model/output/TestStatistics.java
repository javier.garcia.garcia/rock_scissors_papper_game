package com.ciklum.challenge.game.model.output;

import com.ciklum.challenge.game.model.base.Hand;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class TestStatistics {

    @Test
    public void testShouldCalculateRightStatistics() {
        MatchInfo win = new MatchInfo(new TestHand(), new TestHand(), Result.PLAYER_1_WINS);
        MatchInfo loss = new MatchInfo(new TestHand(), new TestHand(), Result.PLAYER_1_LOSS);
        MatchInfo draw = new MatchInfo(new TestHand(), new TestHand(), Result.DRAW);
        Statistics statistics = new Statistics(List.of(win,loss,draw));
        assertThat(statistics.toString(), equalTo("3 games, Player 1 won 1(33.33%), lost 1(33.33%) and tie 1(33.33%)"));
    }

    @Test
    public void testShouldCalculateRightPercentage() {
        MatchInfo win1 = new MatchInfo(new TestHand(), new TestHand(), Result.PLAYER_1_WINS);
        MatchInfo win2 = new MatchInfo(new TestHand(), new TestHand(), Result.PLAYER_1_WINS);
        MatchInfo loss = new MatchInfo(new TestHand(), new TestHand(), Result.PLAYER_1_LOSS);
        Statistics statistics = new Statistics(List.of(win1, win2 ,loss));
        assertThat(statistics.toString(), equalTo("3 games, Player 1 won 2(66.67%), lost 1(33.33%) and tie 0(0%)"));
    }

    private class TestHand implements Hand {

    }
}
