package com.ciklum.challenge.game.model.rspls;

import com.ciklum.challenge.game.utils.IntegerRandomGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.ciklum.challenge.game.model.rspls.RSPSLHand.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@ExtendWith(MockitoExtension.class)
public class TestRSPLSRandomPlayer {
    @Mock
    private IntegerRandomGenerator randomGeneratorMock;

    @InjectMocks
    private RSPLSRandomPlayer underTest;

    @Test
    public void testShouldPlayerReturnRock() {
        Mockito.when(randomGeneratorMock.get(5)).thenReturn(0);
        assertThat(underTest.play(), equalTo(ROCK));
    }

    @Test
    public void testShouldPlayerReturnScissors() {
        Mockito.when(randomGeneratorMock.get(5)).thenReturn(1);
        assertThat(underTest.play(), equalTo(SCISSORS));
    }

    @Test
    public void testShouldPlayerReturnPapper() {
        Mockito.when(randomGeneratorMock.get(5)).thenReturn(2);
        assertThat(underTest.play(), equalTo(PAPPER));
    }

    @Test
    public void testShouldPlayerReturnLizzard() {
        Mockito.when(randomGeneratorMock.get(5)).thenReturn(3);
        assertThat(underTest.play(), equalTo(LIZZARD));
    }

    @Test
    public void testShouldPlayerReturnSpock() {
        Mockito.when(randomGeneratorMock.get(5)).thenReturn(4);
        assertThat(underTest.play(), equalTo(SPOCK));
    }
}
