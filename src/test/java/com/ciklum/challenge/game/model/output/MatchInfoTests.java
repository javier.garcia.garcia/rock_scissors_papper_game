package com.ciklum.challenge.game.model.output;

import com.ciklum.challenge.game.model.rsp.RSPHand;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class MatchInfoTests {

    @Test
    public void testShouldGenerateDrawMessage() {
        MatchInfo result = new MatchInfo(RSPHand.ROCK, RSPHand.ROCK, Result.DRAW);
        assertThat(result.toString(), matchesRegex("Players draw\\s+Fixed player played ROCK\\s+Random player played ROCK\\s+"));
    }

    @Test
    public void testShouldGenerateFixedPlayerWinsMessage() {
        MatchInfo result = new MatchInfo(RSPHand.ROCK, RSPHand.SCISSORS, Result.PLAYER_1_WINS);
        assertThat(result.toString(), matchesRegex("Fixed player wins\\s+Fixed player played ROCK\\s+Random player played SCISSORS\\s+"));
    }

    @Test
    public void testShouldGenerateFixedPlayerLossMessage() {
        MatchInfo result = new MatchInfo(RSPHand.ROCK, RSPHand.PAPPER, Result.PLAYER_1_LOSS);
        assertThat(result.toString(), matchesRegex("Fixed player loss\\s+Fixed player played ROCK\\s+Random player played PAPPER\\s+"));
    }

    @Test
    public void testShouldCompareRight() {
        MatchInfo m1 = new MatchInfo(RSPHand.ROCK, RSPHand.PAPPER, Result.PLAYER_1_LOSS);
        MatchInfo m2 = new MatchInfo(RSPHand.ROCK, RSPHand.PAPPER, Result.PLAYER_1_LOSS);
        assertThat(m1.equals(m2), equalTo(true));
    }

    @Test
    public void testShouldCompareSameElementAndReturnTrue() {
        MatchInfo m1 = new MatchInfo(RSPHand.ROCK, RSPHand.PAPPER, Result.PLAYER_1_LOSS);
        assertThat(m1.equals(m1), equalTo(true));
    }

    @Test
    public void testShouldCompareDifferntElementAndReturnFalse() {
        MatchInfo m1 = new MatchInfo(RSPHand.ROCK, RSPHand.PAPPER, Result.PLAYER_1_LOSS);
        assertThat(m1.equals("String"), equalTo(false));
    }

    @Test
    public void testShouldCompareDifferntInfosAndReturnFalseWhenFixedPlayerReturnsDifferent() {
        MatchInfo m1 = new MatchInfo(RSPHand.ROCK, RSPHand.PAPPER, Result.PLAYER_1_LOSS);
        MatchInfo m2 = new MatchInfo(RSPHand.SCISSORS, RSPHand.PAPPER, Result.PLAYER_1_LOSS);
        assertThat(m1.equals(m2), equalTo(false));
    }

    @Test
    public void testShouldCompareDifferntInfosAndReturnFalseWhenRandomPlayerReturnsDifferent() {
        MatchInfo m1 = new MatchInfo(RSPHand.ROCK, RSPHand.PAPPER, Result.PLAYER_1_LOSS);
        MatchInfo m2 = new MatchInfo(RSPHand.ROCK, RSPHand.SCISSORS, Result.PLAYER_1_LOSS);
        assertThat(m1.equals(m2), equalTo(false));
    }

    @Test
    public void testShouldCompareDifferntInfosAndReturnFalseWhenResultIsDifferent() {
        MatchInfo m1 = new MatchInfo(RSPHand.ROCK, RSPHand.PAPPER, Result.PLAYER_1_LOSS);
        MatchInfo m2 = new MatchInfo(RSPHand.ROCK, RSPHand.PAPPER, Result.DRAW);
        assertThat(m1.equals(m2), equalTo(false));
    }

    @Test
    public void testShouldGenerateAHashCode() {
        MatchInfo m1 = new MatchInfo(RSPHand.ROCK, RSPHand.PAPPER, Result.PLAYER_1_LOSS);
        assertThat(m1.hashCode(), notNullValue());
    }

    @Test
    public void testShouldGenerateDifferentHashCodes() {
        MatchInfo m1 = new MatchInfo(RSPHand.ROCK, RSPHand.PAPPER, Result.PLAYER_1_LOSS);
        MatchInfo m2 = new MatchInfo(RSPHand.PAPPER, RSPHand.PAPPER, Result.DRAW);
        assertThat(m1.hashCode(), not(equalTo(m2.hashCode())));
    }
}
