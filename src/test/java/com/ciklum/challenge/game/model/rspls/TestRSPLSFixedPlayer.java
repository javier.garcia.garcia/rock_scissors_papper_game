package com.ciklum.challenge.game.model.rspls;

import com.ciklum.challenge.game.model.exceptions.GameException;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static com.ciklum.challenge.game.model.rspls.RSPSLHand.SPOCK;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestRSPLSFixedPlayer {
    @Test
    public void testShouldFixedPlayerReturnScissors() {
        RSPLSFixedPlayer underTest = new RSPLSFixedPlayer("SPOCK", null);
        assertThat(underTest.play(), equalTo(SPOCK));
    }

    @Test
    public void testShouldFixedPlayerReturnRandom() {
        RSPLSRandomPlayer mock = Mockito.mock(RSPLSRandomPlayer.class);
        Mockito.when(mock.play()).thenReturn(SPOCK);
        RSPLSFixedPlayer underTest = new RSPLSFixedPlayer("RANDOM", mock);
        assertThat(underTest.play(), equalTo(SPOCK));
        Mockito.verify(mock).play();
    }

    @Test
    public void testShouldFailWhenInvalidFixedValue() {
        Exception exception = assertThrows(GameException.class, () -> new RSPLSFixedPlayer("A", null));
        String expectedMessage = "Fixed value must be ROCK, SCISSORS, PAPPER, LIZZARD, SPOCK or RANDOM";
        String actualMessage = exception.getMessage();

        assertThat(actualMessage, equalTo(expectedMessage));
    }
}
