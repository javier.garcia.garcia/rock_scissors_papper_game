package com.ciklum.challenge.game.model.rspls;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;

public class TestRSPLSHand {
    @Test
    public void testShouldReturRock() {
        assertThat(RSPSLHand.from(0), equalTo(RSPSLHand.ROCK));
    }

    @Test
    public void testShouldReturScissors() {
        assertThat(RSPSLHand.from(1), equalTo(RSPSLHand.SCISSORS));
    }

    @Test
    public void testShouldReturPapper() { assertThat(RSPSLHand.from(2), equalTo(RSPSLHand.PAPPER)); }

    @Test
    public void testShouldReturLizzard() { assertThat(RSPSLHand.from(3), equalTo(RSPSLHand.LIZZARD)); }

    @Test
    public void testShouldReturSpock() { assertThat(RSPSLHand.from(4), equalTo(RSPSLHand.SPOCK)); }

    @Test
    public void testShouldReturNull() {
        assertThat(RSPSLHand.from(5), nullValue());
    }
}
