package com.ciklum.challenge.game;

import com.ciklum.challenge.game.model.base.Hand;
import com.ciklum.challenge.game.model.base.Judge;
import com.ciklum.challenge.game.model.base.Player;
import com.ciklum.challenge.game.model.output.MatchInfo;
import com.ciklum.challenge.game.model.output.Result;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class TestMatch {

    private Player randomPlayerMock;
    private Player fixedPlayerMock;
    private Judge judgeMock;

    private Match underTest;

    @BeforeEach
    public void setUp() {
        this.randomPlayerMock = Mockito.mock(Player.class);
        this.fixedPlayerMock = Mockito.mock(Player.class);
        this.judgeMock = Mockito.mock(Judge.class);
        this.underTest = new Match(this.randomPlayerMock, this.fixedPlayerMock, this.judgeMock);
    }

    @Test
    public void testShouldMatchCallBeans() {
        TestHand mockHand1 = new TestHand();
        TestHand mockHand2 = new TestHand();
        MatchInfo expectedResult = new MatchInfo(mockHand2, mockHand1, Result.DRAW);
        Mockito.when(randomPlayerMock.play()).thenReturn(mockHand1);
        Mockito.when(fixedPlayerMock.play()).thenReturn(mockHand2);
        Mockito.when(judgeMock.decide(Mockito.any(), Mockito.any())).thenReturn(Result.DRAW);
        assertThat(underTest.play(), equalTo(expectedResult));
        Mockito.verify(randomPlayerMock).play();
        Mockito.verify(fixedPlayerMock).play();
        Mockito.verify(judgeMock).decide(mockHand2, mockHand1);
    }

    private static class TestHand implements Hand {

    }
}

