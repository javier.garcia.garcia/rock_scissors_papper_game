package com.ciklum.challenge.game;

import com.github.blindpirate.extensions.CaptureSystemOutput;
import org.junit.jupiter.api.Test;

import static org.hamcrest.Matchers.containsString;

class GameApplicationIntegrationTests {

	@Test
	@CaptureSystemOutput
	public void testShouldPlayRockScissorsPapper(CaptureSystemOutput.OutputCapture outputCapture) {
		GameApplication.main(new String [] { "--numExecutions=1", "--fixedValue=ROCK" });
		outputCapture.expect(containsString("Game in mode: RSP, number of executions: 1"));
		outputCapture.expect(containsString("1 games, Player 1 won "));
		outputCapture.expect(containsString("Fixed player played ROCK"));
	}

	@Test
	@CaptureSystemOutput
	public void testShouldPlayRockScissorsPapperLizzardSpock(CaptureSystemOutput.OutputCapture outputCapture) {
		GameApplication.main(new String [] { "--numExecutions=1", "--fixedValue=SPOCK", "--gameMode=RSPLS" });
		outputCapture.expect(containsString("Fixed player played SPOCK"));
		outputCapture.expect(containsString("1 games, Player 1 won "));
		outputCapture.expect(containsString("Game in mode: RSPLS, number of executions: 1"));
	}
}
