package com.ciklum.challenge.game;

import com.ciklum.challenge.game.model.base.Hand;
import com.ciklum.challenge.game.model.exceptions.GameException;
import com.ciklum.challenge.game.model.output.MatchInfo;
import com.ciklum.challenge.game.model.output.Result;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class GameApplicationTests {
    private final Integer numExecutions = 5;

    private Match matchMock;
    private String gameMode;
    private GameApplication gameApplication;

    @BeforeEach
    public void setUp() {
        this.gameMode = "RSP";
        this.matchMock = Mockito.mock(Match.class);
        this.gameApplication = new GameApplication(numExecutions, this.gameMode, this.matchMock);
    }

    @Test
    public void testShouldCallMatchSeveralTimes() {
        Mockito.when(this.matchMock.play()).thenReturn(new MatchInfo(new TestHand(), new TestHand(), Result.DRAW));
        this.gameApplication.run();
        Mockito.verify(this.matchMock, Mockito.times(numExecutions)).play();
    }

    @Test
    public void testShouldFailWhenWrongNumberOfExecutions() {
        Exception exception = assertThrows(GameException.class, () -> new GameApplication(0, "RSP", this.matchMock));
        String expectedMessage = "Number of execution must be a positive integer";
        String actualMessage = exception.getMessage();

        assertThat(actualMessage, equalTo(expectedMessage));
    }

    @Test
    public void testShouldFailWhenWrongGameMode() {
        Exception exception = assertThrows(GameException.class, () -> new GameApplication(1, "AA", this.matchMock));
        String expectedMessage = "Invalid game mode. Must be RSP or RSPSL";
        String actualMessage = exception.getMessage();

        assertThat(actualMessage, equalTo(expectedMessage));
    }

    private static class TestHand implements Hand {
    }
}
