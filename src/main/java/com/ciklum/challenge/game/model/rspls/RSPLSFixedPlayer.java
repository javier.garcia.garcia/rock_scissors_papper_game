package com.ciklum.challenge.game.model.rspls;

import com.ciklum.challenge.game.GameApplication;
import com.ciklum.challenge.game.model.base.Hand;
import com.ciklum.challenge.game.model.base.Player;
import com.ciklum.challenge.game.model.exceptions.GameException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;


@Component("fixedPlayer")
@ConditionalOnProperty(value="gameMode", havingValue = GameApplication.RSPSL_MODE, matchIfMissing = false)
public class RSPLSFixedPlayer implements Player {

    private String fixedValue;
    private RSPLSRandomPlayer randomPlayer;

    public RSPLSFixedPlayer(@Value("${fixedValue:SPOCK}") String fixedValue,
                            @Autowired  RSPLSRandomPlayer randomPlayer) {
        this.fixedValue = fixedValue;
        this.randomPlayer = randomPlayer;
        this.validateInput();
    }

    private void validateInput() {
        try {
            if (!"RANDOM".equals(fixedValue)) {
                RSPSLHand.valueOf(this.fixedValue);
            }
        } catch (IllegalArgumentException e) {
            throw new GameException("Fixed value must be ROCK, SCISSORS, PAPPER, LIZZARD, SPOCK or RANDOM");
        }
    }

    @Override
    public Hand play() {
        if ("RANDOM".equals(fixedValue)) {
            return randomPlayer.play();
        }
        return RSPSLHand.valueOf(fixedValue.toUpperCase());
    }
}
