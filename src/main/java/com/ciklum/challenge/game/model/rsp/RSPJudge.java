package com.ciklum.challenge.game.model.rsp;

import com.ciklum.challenge.game.GameApplication;
import com.ciklum.challenge.game.model.base.Hand;
import com.ciklum.challenge.game.model.base.Judge;
import com.ciklum.challenge.game.model.exceptions.GameException;
import com.ciklum.challenge.game.model.output.Result;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

@Component("judge")
@ConditionalOnProperty(value="gameMode", havingValue = GameApplication.RSP_MODE, matchIfMissing = true)
public class RSPJudge implements Judge {
    @Override
    public Result decide(Hand player1Hand, Hand player2Hand) throws GameException {
        if (player1Hand instanceof RSPHand && player2Hand instanceof RSPHand) {
            RSPHand p1 = (RSPHand) player1Hand;
            RSPHand p2 = (RSPHand) player2Hand;
            if(p1.winAgainst().contains(p2)) {
                return Result.PLAYER_1_WINS;
            } else if (p1.loseAgainst().contains(p2)) {
                return Result.PLAYER_1_LOSS;
            }
            return Result.DRAW;
        }
        throw new GameException("Invalid hand");
    }
}
