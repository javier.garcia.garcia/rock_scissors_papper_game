package com.ciklum.challenge.game.model.output;

import com.ciklum.challenge.game.model.base.Hand;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Objects;

@AllArgsConstructor
public class MatchInfo {
    private Hand fixedPlayerHand;
    private Hand randomPlayerHand;

    @Getter
    private Result result;

    @Override
    public String toString() {
        return String.format("%1$-20s Fixed player played %2$-10s Random player played %3$-10s",
                this.translateResult(), this.fixedPlayerHand, this.randomPlayerHand);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MatchInfo)) return false;
        MatchInfo matchInfo = (MatchInfo) o;
        return Objects.equals(fixedPlayerHand, matchInfo.fixedPlayerHand) &&
                Objects.equals(randomPlayerHand, matchInfo.randomPlayerHand) &&
                result == matchInfo.result;
    }

    @Override
    public int hashCode() {
        return 31*super.hashCode();
    }

    private String translateResult() {
        return switch (this.result) {
            case DRAW: yield "Players draw";
            case PLAYER_1_WINS: yield "Fixed player wins";
            default: yield "Fixed player loss";
        };
    }
}
