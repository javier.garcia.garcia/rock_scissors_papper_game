package com.ciklum.challenge.game.model.output;

public enum Result {
    PLAYER_1_WINS ,
    PLAYER_1_LOSS,
    DRAW
}
