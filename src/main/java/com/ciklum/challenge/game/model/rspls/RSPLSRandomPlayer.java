package com.ciklum.challenge.game.model.rspls;

import com.ciklum.challenge.game.GameApplication;
import com.ciklum.challenge.game.model.base.Hand;
import com.ciklum.challenge.game.model.base.Player;
import com.ciklum.challenge.game.utils.IntegerRandomGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

@Component("randomPlayer")
@ConditionalOnProperty(value="gameMode", havingValue = GameApplication.RSPSL_MODE, matchIfMissing = false)
public class RSPLSRandomPlayer implements Player {
    @Autowired
    IntegerRandomGenerator randGenerator;

    @Override
    public Hand play() {
        Integer randValue = randGenerator.get(RSPSLHand.values().length);
        return RSPSLHand.from(randValue);
    }
}
