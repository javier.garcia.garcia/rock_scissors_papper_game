package com.ciklum.challenge.game.model.rsp;

import com.ciklum.challenge.game.GameApplication;
import com.ciklum.challenge.game.model.base.Hand;
import com.ciklum.challenge.game.model.base.Player;
import com.ciklum.challenge.game.model.exceptions.GameException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

@Component("fixedPlayer")
@ConditionalOnProperty(value="gameMode", havingValue = GameApplication.RSP_MODE, matchIfMissing = true)
public class RSPFixedPlayer implements Player {

    private String fixedValue;
    private Player randomPlayer;

    public RSPFixedPlayer(@Value("${fixedValue:ROCK}") String value,
                          @Autowired Player randomPlayer) {
        this.fixedValue = value;
        this.randomPlayer = randomPlayer;
        this.validateInput();
    }

    private void validateInput() {
        try {
            if (!"RANDOM".equals(fixedValue)) {
                RSPHand.valueOf(this.fixedValue);
            }
        } catch (IllegalArgumentException e) {
            throw new GameException("Fixed value must be ROCK, SCISSORS, PAPPER or RANDOM");
        }
    }

    @Override
    public Hand play() {
        if ("RANDOM".equals(fixedValue)) {
            return randomPlayer.play();
        }
        return RSPHand.valueOf(fixedValue);
    }
}
