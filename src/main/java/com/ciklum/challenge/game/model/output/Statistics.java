package com.ciklum.challenge.game.model.output;

import java.text.DecimalFormat;
import java.util.List;

public class Statistics {
    private List<MatchInfo> results;
    private Integer victories = 0;
    private Integer beats = 0;
    private Integer ties = 0;

    public Statistics(List<MatchInfo> results) {
        this.results = results;
        this.calculateNumbers();
    }

    @Override
    public String toString() {
        return String.format("%s games, Player 1 won %s(%s), lost %s(%s) and tie %s(%s)",
                results.size(),
                this.victories,
                this.getPercentage(victories, results.size()),
                this.beats,
                this.getPercentage(beats, results.size()),
                this.ties,
                this.getPercentage(ties, results.size()));
    }

    private void calculateNumbers() {
        for(MatchInfo m : results) {
            switch (m.getResult()) {
                case DRAW -> ties++;
                case PLAYER_1_LOSS -> beats++;
                default -> victories++;
            }
        }
    }

    private String getPercentage(Integer number, Integer total) {
        DecimalFormat df = new DecimalFormat("#.##%");
        return df.format(number.floatValue()/total);
    }
}
