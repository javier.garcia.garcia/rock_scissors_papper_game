package com.ciklum.challenge.game.model.rsp;

import com.ciklum.challenge.game.model.base.Hand;

import java.util.List;

public enum RSPHand implements Hand {
    ROCK("ROCK", 0) {
        @Override
        public List<RSPHand> winAgainst() {
            return List.of(SCISSORS);
        }

        @Override
        public List<RSPHand> loseAgainst() {
            return List.of(PAPPER);
        }
    },
    SCISSORS("SCISSORS", 1) {
        @Override
        public List<RSPHand> winAgainst() {
            return List.of(PAPPER);
        }

        @Override
        public List<RSPHand> loseAgainst() {
            return List.of(ROCK);
        }
    },
    PAPPER("PAPPER", 2) {
        @Override
        public List<RSPHand> winAgainst() {
            return List.of(ROCK);
        }

        @Override
        public List<RSPHand> loseAgainst() {
            return List.of(SCISSORS);
        }
    };

    private String name;
    private Integer value;

    RSPHand(String name, Integer value) {
        this.name = name;
        this.value = value;
    }

    public abstract List<RSPHand> winAgainst();
    public abstract List<RSPHand> loseAgainst();

    public static RSPHand from(Integer value) {
        for (RSPHand entry : values()) {
            if(entry.value.equals(value)) {
                return entry;
            }
        }
        return null;
    }
}
