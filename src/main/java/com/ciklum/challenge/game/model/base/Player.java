package com.ciklum.challenge.game.model.base;

public interface Player {
    Hand play();
}
