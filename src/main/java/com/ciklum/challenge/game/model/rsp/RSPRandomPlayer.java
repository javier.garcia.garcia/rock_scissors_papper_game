package com.ciklum.challenge.game.model.rsp;

import com.ciklum.challenge.game.GameApplication;
import com.ciklum.challenge.game.model.base.Hand;
import com.ciklum.challenge.game.model.base.Player;
import com.ciklum.challenge.game.utils.IntegerRandomGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;


@Component("randomPlayer")
@ConditionalOnProperty(value="gameMode", havingValue = GameApplication.RSP_MODE, matchIfMissing = true)
public class RSPRandomPlayer implements Player {
    @Autowired
    IntegerRandomGenerator randGenerator;

    @Override
    public Hand play() {
        Integer randValue = randGenerator.get(RSPHand.values().length);
        return RSPHand.from(randValue);
    }
}
