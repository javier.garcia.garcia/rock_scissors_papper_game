package com.ciklum.challenge.game.model.rspls;

import com.ciklum.challenge.game.model.base.Hand;

import java.util.List;

public enum RSPSLHand implements Hand {
    ROCK("ROCK", 0) {
        @Override
        public List<RSPSLHand> winAgainst() {
            return List.of(SCISSORS, LIZZARD);
        }

        @Override
        public List<RSPSLHand> loseAgainst() {
            return List.of(PAPPER, SPOCK);
        }
    },
    SCISSORS("SCISSORS", 1) {
        @Override
        public List<RSPSLHand> winAgainst() {
            return List.of(PAPPER, LIZZARD);
        }

        @Override
        public List<RSPSLHand> loseAgainst() {
            return List.of(ROCK, SPOCK);
        }
    },
    PAPPER("PAPPER", 2) {
        @Override
        public List<RSPSLHand> winAgainst() {
            return List.of(ROCK, SPOCK);
        }

        @Override
        public List<RSPSLHand> loseAgainst() {
            return List.of(SCISSORS, LIZZARD);
        }
    },
    LIZZARD("LIZZARD", 3) {
        @Override
        public List<RSPSLHand> winAgainst() {
            return List.of(SPOCK, PAPPER);
        }

        @Override
        public List<RSPSLHand> loseAgainst() {
            return List.of(ROCK, SCISSORS);
        }
    },
    SPOCK("SPOCK", 4) {
        @Override
        public List<RSPSLHand> winAgainst() {
            return List.of(SCISSORS, ROCK);
        }

        @Override
        public List<RSPSLHand> loseAgainst() {
            return List.of(PAPPER, LIZZARD);
        }
    };

    private String name;
    private Integer value;

    RSPSLHand(String name, Integer value) {
        this.name = name;
        this.value = value;
    }

    public abstract List<RSPSLHand> winAgainst();
    public abstract List<RSPSLHand> loseAgainst();

    public static RSPSLHand from(Integer value) {
        for (RSPSLHand entry : values()) {
            if(entry.value.equals(value)) {
                return entry;
            }
        }
        return null;
    }
}
