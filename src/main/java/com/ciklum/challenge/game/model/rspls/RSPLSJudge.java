package com.ciklum.challenge.game.model.rspls;

import com.ciklum.challenge.game.GameApplication;
import com.ciklum.challenge.game.model.base.Hand;
import com.ciklum.challenge.game.model.base.Judge;
import com.ciklum.challenge.game.model.exceptions.GameException;
import com.ciklum.challenge.game.model.output.Result;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

@Component("judge")
@ConditionalOnProperty(value="gameMode", havingValue = GameApplication.RSPSL_MODE, matchIfMissing = false)
public class RSPLSJudge implements Judge {
    @Override
    public Result decide(Hand player1Hand, Hand player2Hand) {
        if (player1Hand instanceof RSPSLHand && player2Hand instanceof RSPSLHand) {
            RSPSLHand p1 = (RSPSLHand) player1Hand;
            RSPSLHand p2 = (RSPSLHand) player2Hand;
            if (p1.winAgainst().contains(p2)) {
                return Result.PLAYER_1_WINS;
            } else if (p1.loseAgainst().contains(p2)) {
                return Result.PLAYER_1_LOSS;
            }
            return Result.DRAW;
        }
        throw new GameException("Invalid hand");
    }
}
