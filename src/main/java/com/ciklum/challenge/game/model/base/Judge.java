package com.ciklum.challenge.game.model.base;

import com.ciklum.challenge.game.model.output.Result;

public interface Judge {
    Result decide(Hand player1Hand, Hand player2Hand);
}
