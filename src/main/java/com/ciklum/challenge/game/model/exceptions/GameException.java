package com.ciklum.challenge.game.model.exceptions;

public class GameException extends RuntimeException {
    public GameException(String message) {
        super(message);
    }
}
