package com.ciklum.challenge.game.utils;

import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class IntegerRandomGenerator {
    public Integer get(Integer bound) {
        return new Random().nextInt(bound);
    }
}
