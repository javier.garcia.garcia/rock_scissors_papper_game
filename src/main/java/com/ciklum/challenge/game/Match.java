package com.ciklum.challenge.game;

import com.ciklum.challenge.game.model.base.Hand;
import com.ciklum.challenge.game.model.base.Judge;
import com.ciklum.challenge.game.model.base.Player;
import com.ciklum.challenge.game.model.output.MatchInfo;
import com.ciklum.challenge.game.model.output.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class Match {
    private Player randomPlayer;
    private Player fixedPlayer;
    private Judge judge;

    public Match(@Autowired @Qualifier("randomPlayer") Player randomPlayer,
                 @Autowired @Qualifier("fixedPlayer") Player fixedPlayer,
                 @Autowired @Qualifier("judge") Judge judge) {
        this.judge = judge;
        this.randomPlayer = randomPlayer;
        this.fixedPlayer = fixedPlayer;
    }

    public MatchInfo play() {
        Hand fixedPlayerHand = fixedPlayer.play();
        Hand randomPlayerHand = randomPlayer.play();
        Result result = judge.decide(fixedPlayerHand, randomPlayerHand);
        return new MatchInfo(fixedPlayerHand, randomPlayerHand, result);
    }
}
