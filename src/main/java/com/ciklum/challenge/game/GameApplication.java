package com.ciklum.challenge.game;

import com.ciklum.challenge.game.model.exceptions.GameException;
import com.ciklum.challenge.game.model.output.MatchInfo;
import com.ciklum.challenge.game.model.output.Statistics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@SpringBootApplication
public class GameApplication implements CommandLineRunner {

	public static final String RSP_MODE = "RSP";
	public static final String RSPSL_MODE = "RSPLS";

	private Integer numExecutions;
	private String gameMode;
	private Match match;

	public GameApplication(@Value("${numExecutions:100}") Integer numExecutions,
						   @Value("${gameMode:RSP}") String gameMode,
						   @Autowired Match match) {
		this.numExecutions = numExecutions;
		this.gameMode = gameMode;
		this.match = match;
		this.validateInput();
	}
	
	private void validateInput() {
		if (this.numExecutions <= 0) {
			throw new GameException("Number of execution must be a positive integer");
		}
		if (!RSP_MODE.equals(this.gameMode) && !RSPSL_MODE.equals(this.gameMode)) {
			throw new GameException("Invalid game mode. Must be RSP or RSPSL");
		}
	}

	@Override
	public void run(final String... args) {
		System.out.println(String.format("Game in mode: %s, number of executions: %s", this.gameMode, this.numExecutions));
		List<MatchInfo> results = IntStream.range(0, numExecutions).mapToObj(i -> match.play()).collect(Collectors.toList());
		Statistics gameStatistics = new Statistics(results);
		System.out.println(gameStatistics);
		results.forEach(System.out::println);
	}

	public static void main(String[] args) {
		SpringApplication.run(GameApplication.class, args);
	}

}
