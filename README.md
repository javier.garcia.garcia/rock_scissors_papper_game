# Rock Scissors Papper Game

## Requisites
* Docker installed

## How to build the app
Run

``make build``

It will generate a docker image containing spring boot application to run the game

## How to run the app
After building it, you can run it by doing

``make run``

It will run the app using the default configuration
* 100 of executions
* One player will always use ROCK as play
* The game will be Rock Scissors Papper (aka RSP)

Output
````
╰─$ make run
docker run ciklum/game_challenge --numExecutions=100 --fixedValue=ROCK --gameMode=RSP

  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::        (v2.2.6.RELEASE)

2020-05-02 10:48:08.294  INFO 45015 --- [           main] c.ciklum.challenge.game.GameApplication  : Starting GameApplication on OEL0127.local with PID 45015 (/Users/javiergarcia/Documents/game/target/classes started by javiergarcia in /Users/javiergarcia/Documents/game)
2020-05-02 10:48:08.296  INFO 45015 --- [           main] c.ciklum.challenge.game.GameApplication  : No active profile set, falling back to default profiles: default
2020-05-02 10:48:09.109  INFO 45015 --- [           main] c.ciklum.challenge.game.GameApplication  : Started GameApplication in 1.222 seconds (JVM running for 1.854)
Game in mode: RSP, number of executions: 10
10 games, Player 1 won 5(50%), lost 3(30%) and tie 2(20%)
Fixed player wins    Fixed player played ROCK       Random player played SCISSORS  
Fixed player loss    Fixed player played ROCK       Random player played PAPPER    
Fixed player loss    Fixed player played ROCK       Random player played PAPPER    
Fixed player loss    Fixed player played ROCK       Random player played PAPPER    
Players draw         Fixed player played ROCK       Random player played ROCK      
Fixed player wins    Fixed player played ROCK       Random player played SCISSORS  
Players draw         Fixed player played ROCK       Random player played ROCK      
Fixed player wins    Fixed player played ROCK       Random player played SCISSORS  
Fixed player wins    Fixed player played ROCK       Random player played SCISSORS  
Fixed player wins    Fixed player played ROCK       Random player played SCISSORS  
````

### Different configurations
You can change the behaviour of the app using the following properties
* NUM_EXECUTIONS: define the number of executions
* FIXED_VALUE: define the value to be used by the first player. Could be ROCK, SCISSORS, PAPPER or RANDOM
    * RANDOM will generate random values
* GAME_MODE: define the game to be played. Could be:
    * RSP: Rock Scissors Papper
    * RSPLS: Rock Scissors Papper Lizzard Spock
    
## Bases of the project
* Project based on Java 14 & Spring Boot 2
* Extensible and prepared to develop new features
* Code desinged and developed following the SOLID principles and clean code rules
* Developed following TDD rules
* 100% of unit test coverage
* 100% of mutation test coverage
* PMD as checkstyle of the project
* Docker as base to build an executable
* Using GitlabCI as Continous Integration
* Using precommit to avoid commit changes without checking PMD checksytle

### Testing
The code has been developed following TDD rules (https://martinfowler.com/bliki/TestDrivenDevelopment.html)
In order to check the right behaviour of the app, the coverage of test has been set to 100% (https://gitlab.com/javier.garcia.garcia/rock_scissors_papper_game/-/blob/master/pom.xml#L127)
The project is using Mutation testing to increase the coverage (https://pitest.org)

You can get the test report in gitlab ci:
https://gitlab.com/javier.garcia.garcia/rock_scissors_papper_game/-/jobs/535456101/artifacts/browse

## Use of precommit
In order to avoid committing wrong changes in code, in terms of style, I've enabled the precommit in git
The check will be executed only when any java file is modified
To do that I've change the precommit command:
```
#!/bin/bash -e
function get_module() {
  local path=$1;
  while true; do
    path=$(dirname $path);
    if [ -f "$path/pom.xml" ]; then
      echo "$path";
      return;
    elif [[ "./" =~ "$path" ]]; then
      return;
    fi
  done
}

modules=();

for file in $(git diff --name-only --cached \*.java); do
  module=$(get_module "$file");
  if [ "" != "$module" ] \
      && [[ ! " ${modules[@]} " =~ " $module " ]]; then
    modules+=("$module");
  fi
done;

if [ ${#modules[@]} -eq 0 ]; then
  exit;
fi

modules_arg=$(printf ",%s" "${modules[@]}");
modules_arg=${modules_arg:1};

export MAVEN_OPTS="-client
  -XX:+TieredCompilation
  -XX:TieredStopAtLevel=1
  -Xverify:none";

mvn -q -pl "$modules_arg" pmd:check;
```